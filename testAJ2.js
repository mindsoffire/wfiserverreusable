const aj = require('./_core/ajExpressMWareEncAlphaNumNHashSalt');
var jwt = require('jwt-simple');

aj.appStartNUseAllMware(6700);              /*                                                                                      */
wfi = aj.appStartNUseAllMware(6701);        /* confirmed can 'load balance' required daemons across all ports listened to here.     */
maybe = aj.appStartNUseAllMware(6702);      /*                                                                                      */

// instance of filesys.
const fs = require('fs');
// require('./_guardHelper/testEncNHash')   // tested encryptions F and G and Hash256.  Also node ./_guardHelper/testEncNHash.js
// require('./_guardHelper/testMyJWT')      // tested my JWT req-res protocol.  Also node ./_guardHelper/testMyJWT.js
const authToken = require('./_guardHelper/authToken');     // stuck here exporting the two functions authToken & genIssueToken.

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'yahoo',
    auth: {
        user: 'mindsoffire@yahoo.com',
        pass: 'A2J4P2T2NaiShan'
    }
});
/* from: 'mindsoffire@yahoo.com',
to: 'mindsoffire@gmail.com',
subject: 'Sending Email using Node.js',
html: '<h1>Welcome</h1><p>That was easy!</p>' */
const SERVERNPORT = 'https://ajverilink.localtunnel.me';
var vURL = '';
var mailOptions = {
    from: 'mindsoffire@yahoo.com',
    to: 'mindsoffire@gmail.com',
    subject: 'Please confirm account',
    html: `Please click on the following link to confirm your account:<p>${vURL}</p>`
    /* text: `Please confirm your account by clicking the following link: ${vURL}` */
}


aj.app.post('/api/register', (req, res) => {
    /*     let clntUserKYC = require('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json'); */
    let clntUserKYC = JSON.parse(fs.readFileSync('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json', 'utf8'));
    let nexToken, found = false;
    console.log('req.body.encID: ', req.body.encID);
    console.log('req.body.hshPW: ', req.body.hshPW);
    /* console.log(JSON.stringify(clntUserKYC)); */
    for (let [index, c] of clntUserKYC.entries()) {
        if (req.body.encID == c.encEmailID) {
            found = true;
            console.log({ found: found, indexKYC: index });
            if (req.body.hshPW !== undefined) {
                if (req.body.hshPW == c.hshPW) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, req.body.hshPW, 'rw_');
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    res.status(200).json({ nexToken: nexToken, status: '..you have already signed up, logging you in.', clntUserKYC: c });
                } else if (c.hshPW != null) {
                    res.status(200).json({ status: '..you have already signed up, please log in with the right password.' });
                } else if (c.hshPW == null) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, req.body.hshPW, 'rw_');
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    c.hshPW = req.body.hshPW;
                    fs.writeFile('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json', JSON.stringify(clntUserKYC), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to new clntUserKYC masterIndexHeadTable jsonDB.");
                            /* console.log(err);
                            console.log(data); */
                            return res.status(500).json({ status: '..it appears we have veriClntUser service write fault, please try again later.' });
                        }
                        console.log("::good:: wrote to new clntUserKYC masterIndexHeadTable jsonDB.");
                        res.status(200).json({ nexToken: nexToken, status: '..you have already subscribed for free, but now you have just signed up! Thank you for that.' });
                    });

                }
            } else if (req.body.hshPW === undefined) {
                if (c.hshPW == null) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, 'null', 'r__');

                        req.astFK = aj.sha256(aj.rotjaG(aj.rotajG(req.body.encID) + '==' + aj.sha256('null')));
                        console.log(req.astFK);
                        /* let clntUserAsset = require(`./${req.astFK}.json`); res.ast = aj.rotajF(JSON.stringify(clntUserAsset)); */
                        let clntUserAsset = JSON.parse(fs.readFileSync(`./${req.astFK}.json`, 'utf8')); res.ast = aj.rotajF(JSON.stringify(clntUserAsset));
                        let testObj = JSON.parse(aj.rotjaF(res.ast));
                        console.log(testObj);
                        console.log(typeof (testObj));
                        console.log(testObj[10]);
                        console.log(testObj[10].Volume);
                        console.log(testObj[10].Name);
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    res.status(200).json({ nexToken: nexToken, status: '..you have already subscribed for free, logging you in.', clntUserAsset: res.ast, resAstDecrypted: aj.rotjaF(res.ast) });
                } else {
                    res.status(200).json({ status: '..our records show you are signed up, please log in with the right password.' });
                }
            }
        } else { }
    }
    if (!found) {
        // send mailto and update tempReg.json
        if (req.body.encID) {
            let tempReg = JSON.parse(fs.readFileSync('./tempReg.json', 'utf8'));
            let foundInTemp = false;

            //create random 16 character token https://www.quora.com/profile/AJ-Funk
            let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            let token = '';
            for (let i = 24; i > 0; --i) {
                token += chars[Math.round(Math.random() * (chars.length - 1))];
            }
            // create expiration date /* var expires = new Date();  expires.setHours(expires.getHours() + 6); */
            let myExpire = Date.now() / 1000 + 6 * 3600;

            for (let [index, uc] of tempReg.entries()) {
                if (req.body.encID == uc.encEmailID) {
                    foundInTemp = true;
                    uc.hshPW = req.body.hshPW ? req.body.hshPW : null;
                    uc.linkToken = {
                        vEmailLinkToken: token,
                        expires: myExpire
                    };
                    vURL = SERVERNPORT + `/api/verilink/?iD=${uc.encEmailID}&vE=${uc.linkToken.vEmailLinkToken}&eX=${uc.linkToken.expires}`;
                }
            }

            if (!foundInTemp) {
                let newClntUser = {};
                /* console.log(tempReg); */
                newClntUser.encEmailID = req.body.encID;
                newClntUser.hshPW = req.body.hshPW ? req.body.hshPW : null;
                newClntUser.emailVerified = false;
                newClntUser.linkToken = {       // save this in your DB https://www.quora.com/profile/AJ-Funk
                    vEmailLinkToken: token,
                    expires: myExpire
                };
                vURL = SERVERNPORT + `/api/verilink/?iD=${newClntUser.encEmailID}&vE=${newClntUser.linkToken.vEmailLinkToken}&eX=${newClntUser.linkToken.expires}`;
                tempReg.push(newClntUser);
            }

            mailOptions.to = aj.rotjaF(req.body.encID);
            let registerType = req.body.hshPW ? 'signup' : 'free subscription';
            /* mailOptions.text = `Please confirm your account by clicking the following link: ${vURL}`; */
            mailOptions.html = `<h2>Please click on the following link to confirm your <strong>${registerType}</strong>:</h2><p>${vURL}</p>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#303030" c-style="not3BG">
            <tbody><tr mc:repeatable="">
                <td align="center" style="background-image: url('https://ajverilink.localtunnel.me/images/Rachela_Asciified.png'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;" c-style="not3BG" id="not3">
                    <div mc:hideable="">

                        <!-- Mobile Wrapper -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody><tr>
                                <td width="100%" align="center">

                                    <div class="sortable_inner">
                                        <!-- Space -->
                                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                            <tbody><tr>
                                                <td width="352" height="30"></td>
                                            </tr>
                                        </tbody></table>
                                        <!-- End Space -->

                                        <!-- Space -->
                                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                            <tbody><tr>
                                                <td width="352" height="50"></td>
                                            </tr>
                                        </tbody></table>
                                        <!-- End Space -->
                                    </div>

                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -moz-box-shadow: 5px 5px 5px rgba(68,68,68,0.6); -webkit-box-shadow: 5px 5px 5px rgba(68,68,68,0.6); box-shadow: 0px 0px 7px rgba(68,68,68,0.2);">
                                        <tbody><tr>
                                            <td width="352" valign="middle" align="center">


                                                <div class="sortable_inner">
                                                    <!-- Start Top -->
                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" style="-webkit-border-top-left-radius: 5px; -moz-border-top-left-radius: 5px; border-top-left-radius: 5px; -webkit-border-top-right-radius: 5px; -moz-border-top-right-radius: 5px; border-top-right-radius: 5px;" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <!-- Header Text -->
                                                                <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                        <td width="100%" height="30"></td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="not3Text" class="fullCenter" mc:edit="15" object="text-editable">
                                                                            <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>May 2014</singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                        <td width="100%" height="40"></td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 43px; color: #3f4345; line-height: 48px;" t-style="not3Headline" class="fullCenter" mc:edit="16" object="text-editable">
                                                                            <!--[if !mso]><!--><span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>Thank you...</singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                        <td width="100%" height="45"></td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                                <tbody><tr>
                                                                                    <td width="100%" height="30"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 30px; color: #3f4345; line-height: 34px;" t-style="not3Text" class="fullCenter" mc:edit="17" object="text-editable">
                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>...for being our customer.</singleline><!--[if !mso]><!--></span>
                                                                                        <!--<![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                                <tbody><tr>
                                                                                    <td width="100%" height="30"></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" align="center">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                                <tbody><tr>
                                                                                    <td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="not3Text" class="fullCenter" mc:edit="18" object="text-editable">
                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore </singleline><!--[if !mso]><!--></span>
                                                                                        <!--<![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                                <tbody><tr>
                                                                                    <td width="100%" height="40"></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <!----------------- Button Center ----------------->
                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" align="center" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                                <tbody><tr>
                                                                                    <td>
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                            <tbody><tr>
                                                                                                <td align="center" height="40" c-style="notBut1BG" bgcolor="#f0f0f0" style="-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: #222020;" t-style="notBut1Text" mc:edit="19">
                                                                                                    <multiline>
                                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                                    <a href="#" style="color: #222020; font-size:16px; text-decoration: none; line-height:34px; width:100%;" object="link-editable" t-style="notBut1Text">Emailaddress</a>
                                                                                <!--[if !mso]><!--></span>
                                                                                                        <!--<![endif]-->
                                                                                                    </multiline>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                    <!----------------- End Button Center ----------------->

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" align="center">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                                <tbody><tr>
                                                                                    <td width="100%" height="15"></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" align="center" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                                <!----------------- Button Center ----------------->
                                                                                <tbody><tr>
                                                                                    <td>
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                            <tbody><tr>
                                                                                                <td align="center" height="40" c-style="notBut2BG" bgcolor="#3f4345" style="-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: #ffffff;" t-style="notBut2Text" mc:edit="20">
                                                                                                    <multiline>
                                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                                    <a href="#" style="color: #ffffff; font-size:16px; text-decoration: none; line-height:34px; width:100%;" t-style="notBut2Text" object="link-editable">Send</a>
                                                                                <!--[if !mso]><!--></span>
                                                                                                        <!--<![endif]-->
                                                                                                    </multiline>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                                <tbody><tr>
                                                                                    <td width="100%" height="40"></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                                <tbody><tr>
                                                                                    <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" class="fullCenter" mc:edit="21" object="text-editable">

                                                                                        <multiline>
                                                                                            <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;" t-style="not3Text"><!--<![endif]-->
                                                                                or do something else
                                                                            <!--[if !mso]><!--></span>
                                                                                            <!--<![endif]-->

                                                                                            <!--[if !mso]><!--><span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                            <a href="#" style="color: #3f4345;" t-style="not3Text">here</a>
                                                                            
                                                                            <!--[if !mso]><!--></span>
                                                                                            <!--<![endif]-->
                                                                                        </multiline>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" bgcolor="#fdba30" c-style="yellowBG" style="-webkit-border-bottom-bottom-radius: 5px; -moz-border-bottom-left-radius: 5px; border-bottom-left-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-bottom-right-radius: 5px; border-bottom-right-radius: 5px;" class="pad15">

                                                                            <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                                <tbody><tr>
                                                                                    <td width="100%" height="40"></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" style="-webkit-border-bottom-left-radius: 5px; -moz-border-bottom-left-radius: 5px; border-bottom-left-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-bottom-right-radius: 5px; border-bottom-right-radius: 5px;" object="drag-module-small">
                                                        <tbody><tr>
                                                            <td width="352" valign="middle" align="center">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                                    <tbody><tr>
                                                                        <td width="265" height="50"></td>
                                                                    </tr>
                                                                </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                    <!-- End Top -->
                                                </div>

                                            </td>
                                        </tr>
                                    </tbody></table>

                                    <div class="sortable_inner">
                                        <!-- Space -->
                                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                            <tbody><tr>
                                                <td width="352" height="30"></td>
                                            </tr>
                                        </tbody></table>
                                        <!-- End Space -->
                                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                            <tbody><tr>
                                                <td width="352" valign="middle" align="center">

                                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                        <tbody><tr>
                                                            <td width="352" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="unsub2" class="fullCenter" mc:edit="21_736372">
                                                                <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica;"><!--<![endif]--><!--subscribe--><a href="#" style="text-decoration: none; color: #3f4345;" t-style="unsub2" object="link-editable">Unsubscribe</a><!--unsub--><!--[if !mso]><!--></span>
                                                                <!--<![endif]-->
                                                                <span object="text-editable"><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica;"><!--<![endif]--><singleline>now</singleline><!--[if !mso]><!--></span>
                                                                <!--<![endif]-->
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                </td>
                                            </tr>
                                        </tbody></table>
                                        <!-- Space -->
                                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                            <tbody><tr>
                                                <td width="352" height="50"></td>
                                            </tr>
                                            <tr>
                                                <td width="352" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody></table>
                                        <!-- End Space -->
                                    </div>

                                </td>
                            </tr>
                        </tbody></table>

                    </div>
                </td>
            </tr>
            </tbody></table>`;

            console.log({ mailOptions: mailOptions, vURL: vURL });
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                    fs.writeFile("./tempReg.json", JSON.stringify(tempReg), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to temp newClntUser register jsonDB.");
                            /* console.log(err);
                            console.log(data); */
                            return res.status(500).json({ status: '..it appears we have newClntUser service write fault, please try again later.' });
                        }
                        console.log("::good:: wrote to temp newClntUser register jsonDB.");
                    });
                }
            });

            if (req.body.hshPW) {
                res.status(200).json({ status: '..thank you for signing up - please check your email to verify within 6 hours.' });
            } else {
                res.status(200).json({ status: '..thank you for subscribing to our free, capitalist-outsource and wealth-growing automated butler service - please check your email to verify within 6 hours..' });
            }
        }
    }
});


aj.app.post('/api/login', (req, res) => {
    let clntUserKYC = JSON.parse(fs.readFileSync('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json', 'utf8'));
    let nexToken, found = false;
    console.log('req.body.encID: ', req.body.encID);
    console.log('req.body.hshPW: ', req.body.hshPW);
    /*         console.log(JSON.stringify(clntUserKYC));  */
    for (let [index, c] of clntUserKYC.entries()) {
        if (req.body.encID == c.encEmailID) {
            found = true;
            console.log({ found: found, indexKYC: index });
            if (req.body.hshPW !== undefined) {
                if (req.body.hshPW == c.hshPW) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, req.body.hshPW, 'rw_');
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    res.status(200).json({ nexToken: nexToken, status: '..welcome, you are now logged in.', clntUserKYC: c });
                } else if (c.hshPW != null) {
                    res.status(200).json({ status: '..please log in with the right password.' });
                } else if (c.hshPW == null) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, 'null', 'r__');

                        req.astFK = aj.sha256(aj.rotjaG(aj.rotajG(req.body.encID) + '==' + aj.sha256('null')));
                        console.log(req.astFK);
                        let clntUserAsset = JSON.parse(fs.readFileSync(`./${req.astFK}.json`, 'utf8')); res.ast = aj.rotajF(JSON.stringify(clntUserAsset));
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    res.status(200).json({ nexToken: nexToken, status: '..you have already subscribed for free, logging you in.', clntUserAsset: res.ast, resAstDecrypted: aj.rotjaF(res.ast) });
                }
            } else if (req.body.hshPW === undefined) {
                if (c.hshPW == null) {
                    try {
                        nexToken = aj.genIssueToken(req.body.encID, 'null', 'r__');

                        req.astFK = aj.sha256(aj.rotjaG(aj.rotajG(req.body.encID) + '==' + aj.sha256('null')));
                        console.log(req.astFK);
                        let clntUserAsset = JSON.parse(fs.readFileSync(`./${req.astFK}.json`, 'utf8')); res.ast = aj.rotajF(JSON.stringify(clntUserAsset));
                    } catch (error) {
                        res.status(400).json({ status: '..oops something broke, please try again.' });
                    }
                    res.status(200).json({ nexToken: nexToken, status: '..welcome, you are now logged into your free wealthmore-meter test account.', clntUserAsset: res.ast, resAstDecrypted: aj.rotjaF(res.ast) });
                } else {
                    res.status(200).json({ status: '..our records show you are signed up, please log in with the right password.' });
                }
            }
        } else { }
    }
    if (!found) {
        res.status(400).json({ status: '..please register by signing up to our carefree, capitalist-outsource and wealth-growing automated butler service, or subscribe to try us for free!' });
    }
});

aj.app.get('/api/verilink', (req, res) => {
    let tempReg = JSON.parse(fs.readFileSync('./tempReg.json', 'utf8'));
    let newClntUser = {};
    let tocFound = false;
    console.log({ iD: req.query.iD, vE: req.query.vE, eX: req.query.eX });
    for (let [index, toc] of tempReg.entries()) {
        if (req.query.iD == toc.encEmailID) {
            if (req.query.vE == toc.linkToken.vEmailLinkToken && req.query.eX == toc.linkToken.expires && !toc.linkToken.emailVerified) {
                let chkExpires = Date.now() / 1000;
                console.log({ chkExpires: chkExpires, linkTokenExpires: toc.linkToken.expires });
                if (chkExpires < toc.linkToken.expires) {
                    let clntUserKYC = JSON.parse(fs.readFileSync('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json', 'utf8'));
                    let veriClntUser = {
                        "encEmailID": null,
                        "hshPW": null,
                        "subDate": null,
                        "clntCreatDate": null,
                        "clntValKYCDate": null,
                        "encLegalNameID": null,
                        "legalIDCred": null,
                        "photo": {
                            "photoIDFrontURL": null,
                            "photoIDBackURL": null,
                            "photoIDsURLsDated": null,
                            "recentFaceVerifiedIDURL": null,
                            "recentFaceVerifiedIDURLDated": null
                        },
                        "gender": null,
                        "DOB": null,
                        "nationality": null,
                        "address": {
                            "postCode": null,
                            "postStreet": null,
                            "postBlock": null,
                            "postUnit": null
                        },
                        "mobileNumID": null,
                        "bank": {
                            "bankName": null,
                            "bankScanStatemtURL": null,
                            "bankScanStatemtURLDated": null,
                            "bankScanStatemtBal": null,
                            "bankAcct": null
                        },
                        "textAnnotNotaryOthers": null,
                        "clntNotes": null,
                        "isDeleted": null
                    };
                    veriClntUser.encEmailID = toc.encEmailID;
                    veriClntUser.hshPW = toc.hshPW;
                    clntUserKYC.push(veriClntUser);
                    fs.writeFile('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json', JSON.stringify(clntUserKYC), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to new clntUserKYC masterIndexHeadTable jsonDB.");
                            /* console.log(err);
                            console.log(data); */
                            return res.status(500).json({ status: '..it appears we have veriClntUser service write fault, please try again later.' });
                        }
                        console.log("::good:: wrote to new clntUserKYC masterIndexHeadTable jsonDB.");
                        toc.emailVerified = true;
                        fs.writeFile('./tempReg.json', JSON.stringify(tempReg), 'utf8', (err) => {
                            if (err) {
                                console.log("::eror:: writing to temp newClntUser register jsonDB.");
                                /* console.log(err);
                                console.log(data); */
                                return res.status(500).json({ status: '..it appears we have veriClntUser service write fault, please try again later.' });
                            }
                            console.log("::good:: wrote to temp newClntUser register jsonDB.");
                        });
                        tocFound = true;
                        res.status(200).sendFile(__dirname + '/public' + '/images/ascii' + '.html');
                    });


                } else {
                    //create random 16 character token https://www.quora.com/profile/AJ-Funk
                    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    let token = '';
                    for (let i = 24; i > 0; --i) {
                        token += chars[Math.round(Math.random() * (chars.length - 1))];
                    }
                    // create expiration date /* var expires = new Date();  expires.setHours(expires.getHours() + 6); */
                    let myExpire = Date.now() / 1000 + 6 * 3600;

                    toc.linkToken = {
                        vEmailLinkToken: token,
                        expires: myExpire
                    };
                    vURL = SERVERNPORT + `/api/verilink/?iD=${toc.encEmailID}&vE=${toc.linkToken.vEmailLinkToken}&eX=${toc.linkToken.expires}`;

                    mailOptions.to = aj.rotjaF(toc.encEmailID);
                    let registerType = toc.hshPW ? 'signup' : 'free subscription';
                    /* mailOptions.text = `Please confirm your account by clicking the following link: ${vURL}`; */
                    mailOptions.html = `<h3>Please click on the following link to confirm your <h1>${registerType}</h1>:</h3><p>${vURL}</p>`;
                    console.log({ mailOptions: mailOptions, vURL: vURL });
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                }
            }
        }
    }
});

aj.app.get('/api/wealthmore-meter', authToken, (req, res) => {

    try {
        let clntUserAsset = JSON.parse(fs.readFileSync(`./${req.astFK}.json`, 'utf8')); res.ast = aj.rotajF(JSON.stringify(clntUserAsset));
        console.log(JSON.stringify(clntUserAsset));
        console.log({ nexToken: res.nexToken, clntUserAsset: res.ast });
        res.status(200).json({ nexToken: res.nexToken, clntUserAsset: res.ast });

    } catch (error) {
        res.status(400).json({ status: '..please try again later..our servers may be overwhelmed.' });
    }
})


// console.log(aj.genIssueToken('jskJIrKKswx@LjHsP.nrj', '3cc98b78aabdbb22273c4679870ad2e05c8672707675734f0ea2fbef6618d651', 'rw_'));
console.log(aj.genIssueToken('jskJIrKKswx@LjHsP.nrj', null, 'rw_'));
// console.log(aj.genIssueToken('jskJIrKKswx@LjHsP.nrj', '', 'rw_'));
console.log(aj.sha256(aj.rotjaG(aj.rotajG('jskJIrKKswx2@LjHsP.nrj') + '==' + aj.sha256('null'))));


aj.app.use(aj.express.static(__dirname + '/public'));

aj.app.use((req, res, next) => {
    res.redirect('/error.html');
}); 
