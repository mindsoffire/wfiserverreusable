const aj = require('../_core/ajExpressMWareEncAlphaNumNHashSalt');
var jwt = require('jwt-simple');

let iat = Date.now();
let jwtPayload = {
    sub: aj.rotajF('mindsoffire@gmail.com') + '##' + aj.sha256('pas#w0Rd'),
    iat: Date.now(),
    exp: iat + 60 * 15 + 60 * Math.floor(Math.random() * 15),
    acl: 'rw_'
}
console.log(jwtPayload);

let subPart = [] = jwtPayload.sub.split(/\#\#/);
console.log(subPart);
let secret = aj.rotajG(subPart[0]) + '==' + aj.sha256(subPart[1]);
console.log('secret: ', secret);

var token = jwt.encode(jwtPayload, secret);
console.log('type of token, token: ', 'jwt : ' + typeof (token), token);

var decode = jwt.decode(token, 'NoVerify', true);
console.log('decoding...', decode);
subPart = decode.sub.split(/\#\#/);
let testSecret = aj.rotajG(subPart[0]) + '==' + aj.sha256(subPart[1]);

try {
    var verified = jwt.decode(token, testSecret);
    console.log({ verified: verified, testSecret: testSecret });
} catch (error) {
    console.debug('return this error with: res.status(401).json(\'authentication failed\')');
    console.error('return this error with: res.status(401).json(\'authentication failed\')');
}
