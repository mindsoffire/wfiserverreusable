const aj = require('../_core/ajExpressMWareEncAlphaNumNHashSalt');

console.log({ 'F on "mindsoffire@gmail.com"': aj.rotajF('mindsoffire@gmail.com'), 'F-1 * F': aj.rotjaF(aj.rotajF('mindsoffire@gmail.com')) });
console.log({ 'F on "mindsoffire1@gmail.com"': aj.rotajF('mindsoffire1@gmail.com'), 'F-1 * F': aj.rotjaF(aj.rotajF('mindsoffire1@gmail.com')) });
console.log({ 'F on "mindsoffire1234@gmail.com"': aj.rotajF('mindsoffire1234@gmail.com'), 'F-1 * F': aj.rotjaF(aj.rotajF('mindsoffire1234@gmail.com')) });
console.log({ 'G on "mindsoffire@gmail.com"': aj.rotajG('mindsoffire@gmail.com'), 'G-1 * G': aj.rotjaG(aj.rotajG('mindsoffire@gmail.com')) });
console.log({ 'G * F on "mindsoffire@gmail.com"': aj.rotajG(aj.rotajF('mindsoffire@gmail.com')), 'F-1 * G-1 * G * F': aj.rotjaF(aj.rotjaG(aj.rotajG(aj.rotajF('mindsoffire@gmail.com')))) });
console.log({ 'G * F on "mindsoffire1234@gmail.com"': aj.rotajG(aj.rotajF('mindsoffire1234@gmail.com')), 'F-1 * G-1 * G * F': aj.rotjaF(aj.rotjaG(aj.rotajG(aj.rotajF('mindsoffire1234@gmail.com')))) });

console.log({ 'sha256F on "pas#w0Rd"': aj.sha256('pas#w0Rd'), 'sha256F * sha256F': aj.sha256(aj.sha256('pas#w0Rd')) });

